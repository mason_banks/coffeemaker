﻿using System;
using System.Collections.Generic;
using Presentation.Actors;
using Presentation.Components;
using Presentation.Consumables;
using Presentation.Drinks;
using Presentation.Exceptions;
using Presentation.Hardware;
using Presentation.Hardware.CoffeeMakers;
using Presentation.Hardware.Components;
using Presentation.Recipes;

namespace CoffeeMakerCustomer
{
    class Program
    {
        static void Main(string[] args)

        {
            introduction();
            var qualityOfEmployee = chooseEmployee();
            var carafeWaterVolume = chooseCarafeWaterVolume();
            var numberOfScoops = chooseNumberOfScoops();

            var drinkOrder = new StandardCoffeeRecipe();

            try
            {
                var barista = (qualityOfEmployee.ToLower() == "good") ? chooseEmployee(true) : chooseEmployee(false);
                var coffee = customerOrder(barista, drinkOrder, carafeWaterVolume, numberOfScoops);
                Console.WriteLine("Your barista made a " + coffee.CoffeeQuality.ToString().ToLower() + " cup of coffee!");
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(ImproperlyConfiguredHardwareException))
                    Console.WriteLine("Wow, your barista didn't put the coffee machine together correctly.");

                if (ex.GetType() == typeof(HardwareCapacityException))
                    Console.WriteLine("You've added 1 or more ingredients that have exceeded or do not meet the minimum requirements for the machine.");
            }

            Console.ReadLine();
        }

        private static void introduction()
        {
            Console.WriteLine("Welcome to the Coffee Shop!");
            Console.WriteLine();
            Console.WriteLine("I'm going to ask you a few questions so that I can make your coffee just the way you like it.");
            Console.WriteLine();
            Console.WriteLine();
        }

        private static string chooseEmployee()
        {
            Console.WriteLine("Tell me, would you like a 'good' employee or a 'bad' one?");
            var input = Console.ReadLine();

            if (input.ToLower() != "good" && input.ToLower() != "bad")
            {
                Console.WriteLine("You have to choose a 'good' or 'bad' employee.  You're choice is invalid");
                chooseEmployee();
            }

            return input;
        }

        private static int chooseCarafeWaterVolume()
        {
            Console.WriteLine("How many cups of water do you want in the carafe?");
            var input = Console.ReadLine();
            var cups = 0;
            try
            {
                cups = int.Parse(input);
            }
            catch (Exception e)
            {
                Console.WriteLine("You've entered a value for carafe quantity that is not valid.  Please try again");
                chooseCarafeWaterVolume();
            }

            return cups;
        }

        private static int chooseNumberOfScoops()
        {
            Console.WriteLine("How many scoops of coffee do you want?");
            var input = Console.ReadLine();
            var scoops = 0;
            try
            {
                scoops = int.Parse(input);
            }
            catch (Exception e)
            {
                Console.WriteLine("You've entered a value for number of scoops that is not valid.  Please try again");
                chooseCarafeWaterVolume();
            }

            return scoops;
        }

        private static Barista chooseEmployee(bool isGoodAtJob)
        {
            var coffeeMakerComponents = prepareCoffeeMakerComponents(isGoodAtJob);
            var preparedCoffeeMaker = prepareCoffeeMaker(coffeeMakerComponents);

            var barista = new Barista(preparedCoffeeMaker);
            return barista;
        }

        private static ICoffee customerOrder(IBarista barista, IRecipe drinkOrder, int cups, int scoops)
        {
            return barista.Brew(drinkOrder, prepareIngredients(cups, scoops));
        }

        private static ICoffeeMakerModel prepareCoffeeMaker(ICoffeeMakerComponents components)
        {
            var coffeeMaker = new CoffeeMaker8Cup(components);
            return coffeeMaker;
        }

        private static Ingredients prepareIngredients(int cups, int scoops)
        {
            var ingridents = new Ingredients
            {
                Collection = new List<IIngredient>
                {
                    new CoffeeGrounds {QuantityInScoops = scoops},
                    new Water {VolumeInCups = cups}
                }
            };

            return ingridents;
        }

        private static CoffeeMakerComponents prepareCoffeeMakerComponents(bool setupCorrectly)
        {
            var coffeeMakerComponents = new CoffeeMakerComponents
            {
                RequiredComponents = setupCorrectly
                    ? new List<IComponent> {new Carafe(), new Filter()}
                    : new List<IComponent> {new Carafe()}
            };

            return coffeeMakerComponents;
        }
    }
}
