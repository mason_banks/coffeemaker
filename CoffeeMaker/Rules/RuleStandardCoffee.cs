﻿using System.Linq;
using Presentation.Consumables;
using Presentation.Drinks;

namespace Presentation.Rules
{
    public class RuleStandardCoffee : IRule
    {
        public CoffeeQuality CalculateQuality(ICoffee coffee)
        {
            var water = (Water)coffee.Ingredients.Collection.ToList().Find(x => x.GetType() == typeof (Water));
            var coffeeGrounds = (CoffeeGrounds)coffee.Ingredients.Collection.ToList().Find(x => x.GetType() == typeof (CoffeeGrounds));

            // Perfect ratio for water to coffee 
            if ((water.VolumeInCups / 2) == coffeeGrounds.QuantityInScoops)
                return CoffeeQuality.Perfect;

            // Too much water, not enough coffee
            if ((water.VolumeInCups / 2) > coffeeGrounds.QuantityInScoops)
                return CoffeeQuality.Weak;

            // Too much coffee, not enough water
            if ((water.VolumeInCups / 2) < coffeeGrounds.QuantityInScoops)
                return CoffeeQuality.Strong;

            return CoffeeQuality.Unknown;
        }

    }
}
