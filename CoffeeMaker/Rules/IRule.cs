﻿using Presentation.Drinks;

namespace Presentation.Rules
{
    public interface IRule
    {
        CoffeeQuality CalculateQuality(ICoffee coffee);
    }
}
