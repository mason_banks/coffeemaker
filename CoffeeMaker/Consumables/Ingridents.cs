﻿using System.Collections.Generic;

namespace Presentation.Consumables
{
    public class Ingredients : IIngredients
    {
        public IList<IIngredient> Collection { get; set; } 
    }

    public interface IIngredients
    {
        IList<IIngredient> Collection { get; set; } 
    }
}
