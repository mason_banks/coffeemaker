﻿namespace Presentation.Consumables
{
    public class Water : IIngredient
    {
        public int Temperature { get; set; }
        public int VolumeInCups { get; set; }
    }
}
