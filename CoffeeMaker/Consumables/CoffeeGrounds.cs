﻿namespace Presentation.Consumables
{
    public class CoffeeGrounds : IIngredient
    {
        public int QuantityInScoops { get; set; }
    } 
}
