﻿using System;

namespace Presentation.Exceptions
{
    public class ImproperlyConfiguredHardwareException : Exception
    {
        public ImproperlyConfiguredHardwareException(string message) : base(message)
        {
        }
    }
}
