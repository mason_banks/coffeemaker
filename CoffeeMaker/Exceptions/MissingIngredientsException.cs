﻿using System;

namespace Presentation.Exceptions
{
    public class MissingIngredientsException : Exception
    {
        public MissingIngredientsException(string message) : base(message)
        {
        }
    }
}
