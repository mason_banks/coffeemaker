﻿using System;

namespace Presentation.Exceptions
{
    public class UnknownComponentException : Exception
    {
        public UnknownComponentException(string message) : base(message)
        {
        }
    }
}
