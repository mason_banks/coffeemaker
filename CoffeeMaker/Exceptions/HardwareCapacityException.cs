﻿using System;

namespace Presentation.Exceptions
{
    public class HardwareCapacityException : Exception
    {
        public HardwareCapacityException(string message) : base(message)
        {
        }
    }
}
