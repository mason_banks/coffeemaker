﻿using Presentation.Consumables;
using Presentation.Drinks;
using Presentation.Hardware.CoffeeMakers;
using Presentation.Recipes;

namespace Presentation.Actors
{
    public class Barista : IBarista
    {
        private readonly ICoffeeMakerModel _coffeeMakerModel;

        public Barista(ICoffeeMakerModel coffeeMakerModel)
        {
            _coffeeMakerModel = coffeeMakerModel;
        }

        public ICoffee Brew(IRecipe recipe, IIngredients ingredients)
        {
            return _coffeeMakerModel.Brew(recipe, ingredients);
        }
    }

    public interface IBarista
    {
        ICoffee Brew(IRecipe recipe, IIngredients ingredients);
    }
}
