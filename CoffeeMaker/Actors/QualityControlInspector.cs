﻿using System;
using System.Linq;
using Presentation.Consumables;
using Presentation.Drinks;
using Presentation.Exceptions;
using Presentation.Hardware;
using Presentation.Hardware.CoffeeMakers;
using Presentation.Hardware.Components;
using Presentation.Recipes;
using Presentation.Rules;

namespace Presentation.Actors
{
    public static class QualityControlInspector
    {
        public static CoffeeQuality InspectBeverageQuality(ICoffee coffee)
        {
            try
            {
                var typeOfQualityRule = coffee.QualityRules.GetType(); // Get the type of the rule
                var rule = (IRule)Activator.CreateInstance(typeOfQualityRule); // use reflection 
                var inspectorReport = rule.CalculateQuality(coffee);

                return inspectorReport;
            }
            catch (Exception e)
            {
                throw new Exception("There was a problem inspecting the coffee.");
            }
        }

        public static bool HardwareIsProperlyConfigured(ICoffeeMakerModel coffeeMakerModel, ICoffeeMakerComponents hardware)
        {
            var hardwareList = hardware.RequiredComponents.ToList();
            foreach (var component in coffeeMakerModel.RequiredComponents)
            {
                var requiredComponentIncluded = hardwareList.Exists(x => x.ToString() == component.ToString());
                if (!requiredComponentIncluded)
                    return false;
            }

            return true;
        }

        public static bool InspectRecipeIngredients(IRecipe recipe, IIngredients ingredients)
        {
            var availableIngredients = ingredients.Collection.ToList();
            foreach (var ingredient in recipe.Ingredients)
            {
                var requiredIngredientIncluded = availableIngredients.Exists(x => x.ToString() == ingredient.ToString());
                if (!requiredIngredientIncluded)
                    return false;
            }

            return true;
        }

        public static bool InspectIngredientsForHardware(ICoffeeMakerModel coffeeMakerModel, IIngredients ingredients)
        {
            foreach (var component in coffeeMakerModel.RequiredComponents)
            {
                var componentType = component.GetType();

                if (componentType == typeof(Filter)) // Inspect the filter
                {
                    var scoopCapicity = ((Filter)component).ScoopCapicity;
                    var coffeeGrounds = (CoffeeGrounds)ingredients.Collection.ToList().Find(x => x.GetType() == typeof(CoffeeGrounds));
                    if (coffeeGrounds.QuantityInScoops > scoopCapicity || coffeeGrounds.QuantityInScoops <= 0)
                        return false;
                }
                else if (componentType == typeof(Carafe)) // Inspect the Carafe
                {
                    var carafeCapacity = ((Carafe) component).MaxVolumeInCups;
                    var water = (Water) ingredients.Collection.ToList().Find(x => x.GetType() == typeof(Water));
                    if (water.VolumeInCups > carafeCapacity || water.VolumeInCups <= 0)
                        return false;
                }
                else
                    throw new UnknownComponentException("A component was added that is not regonized by the inspector.");
            }

            return true;
        }

    }
}
