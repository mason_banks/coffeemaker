﻿using System.Collections.Generic;
using Presentation.Consumables;
using Presentation.Rules;

namespace Presentation.Recipes
{
    public class StandardCoffeeRecipe : IRecipe
    {
        public IList<IIngredient> Ingredients
        {
            get
            {
                var ingredients = new List<IIngredient> {new CoffeeGrounds(), new Water()};

                return ingredients;
            }

        }

        public IRule QualityRule => new RuleStandardCoffee();
    }
}
