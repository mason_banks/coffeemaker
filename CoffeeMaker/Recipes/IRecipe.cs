﻿using System.Collections.Generic;
using Presentation.Consumables;
using Presentation.Rules;

namespace Presentation.Recipes
{
    public interface IRecipe
    {
        IList<IIngredient> Ingredients { get;}
        IRule QualityRule { get; }
    }
}
