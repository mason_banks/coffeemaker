﻿using System.Collections.Generic;
using Presentation.Hardware;
using Presentation.Hardware.Components;

namespace Presentation.Components
{
    public class CoffeeMakerComponents : ICoffeeMakerComponents
    {
        public IList<IComponent> RequiredComponents { get; set; }
    }
}
