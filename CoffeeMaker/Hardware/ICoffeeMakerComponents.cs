﻿using System.Collections.Generic;
using Presentation.Hardware.Components;

namespace Presentation.Hardware
{
    public interface ICoffeeMakerComponents
    {
        IList<IComponent> RequiredComponents { get; set; }
    }
}
