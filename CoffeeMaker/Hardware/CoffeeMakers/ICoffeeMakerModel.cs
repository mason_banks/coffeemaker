﻿using System.Collections.Generic;
using Presentation.Drinks;
using Presentation.Consumables;
using Presentation.Hardware.Components;
using Presentation.Recipes;

namespace Presentation.Hardware.CoffeeMakers
{
    public interface ICoffeeMakerModel
    {
        ICoffee Brew(IRecipe recipe, IIngredients ingridents);
        List<IComponent> RequiredComponents { get; set; }
    }
}
