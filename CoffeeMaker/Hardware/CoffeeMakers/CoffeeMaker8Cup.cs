﻿using System.Collections.Generic;
using Presentation.Actors;
using Presentation.Drinks;
using Presentation.Consumables;
using Presentation.Exceptions;
using Presentation.Hardware.Components;
using Presentation.Recipes;
using Presentation.Rules;

namespace Presentation.Hardware.CoffeeMakers
{
    public class CoffeeMaker8Cup : ICoffeeMakerModel
    {
        private const int CarafeCapicity = 8;
        private const int ScoopCapicity = 8;

        public List<IComponent> RequiredComponents { get; set; }

        public CoffeeMaker8Cup(ICoffeeMakerComponents coffeeMakerComponents)
        {
            setRequiredComponents();
            var isHardwareValid = 
                QualityControlInspector.HardwareIsProperlyConfigured(this, coffeeMakerComponents);

            if (!isHardwareValid)
                throw new ImproperlyConfiguredHardwareException("The hardware is improperly configured.");
        }

        public ICoffee Brew(IRecipe recipe, IIngredients ingredients)
        {
            performQualityCheck(recipe, ingredients);

            ICoffee coffee = new StandardCoffee { Ingredients = ingredients };
            coffee.Ingredients = ingredients;
            coffee.QualityRules = new RuleStandardCoffee();
            coffee.Recipe = recipe;
            coffee.CoffeeQuality = QualityControlInspector.InspectBeverageQuality(coffee);

            return coffee;
        }

        private void performQualityCheck(IRecipe recipe, IIngredients ingredients)
        {
            var hasAllIngredients = QualityControlInspector.InspectRecipeIngredients(recipe, ingredients);

            if (!hasAllIngredients)
                throw new MissingIngredientsException("Ingredients for the requested recipe are missing.");

            var ingredientsAreValidForHardware = QualityControlInspector.InspectIngredientsForHardware(this, ingredients);

            if (!ingredientsAreValidForHardware)
                throw new HardwareCapacityException(
                    "An ingredient has been added that exceeds or doesn't meet the requirements of the hardware.");
        }

        private void setRequiredComponents()
        {
            RequiredComponents = new List<IComponent>
            {
                new Carafe {MaxVolumeInCups = CarafeCapicity},
                new Filter {ScoopCapicity = ScoopCapicity}
            };
        }
    }
}
