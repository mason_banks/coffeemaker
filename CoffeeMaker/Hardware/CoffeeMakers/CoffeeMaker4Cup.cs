﻿using System.Collections.Generic;
using Presentation.Actors;
using Presentation.Consumables;
using Presentation.Drinks;
using Presentation.Exceptions;
using Presentation.Hardware.Components;
using Presentation.Recipes;
using Presentation.Rules;

namespace Presentation.Hardware.CoffeeMakers
{
    public class CoffeeMaker4Cup : ICoffeeMakerModel
    {
        private const int CarafeCapicity = 4;
        private const int ScoopCapicity = 4;

        public List<IComponent> RequiredComponents { get; set; }

        public CoffeeMaker4Cup(ICoffeeMakerComponents coffeeMakerComponents)
        {
            setRequiredComponents();
            var isHardwareValid =
                QualityControlInspector.HardwareIsProperlyConfigured(this, coffeeMakerComponents);

            if (!isHardwareValid)
                throw new ImproperlyConfiguredHardwareException("The hardware is improperly configured.");
        }

        public ICoffee Brew(IRecipe recipe, IIngredients ingredients)
        {
            var hasAllIngredients =
                QualityControlInspector.InspectRecipeIngredients(recipe, ingredients);

            if (!hasAllIngredients)
                throw new MissingIngredientsException("Ingredients for the requested recipe are missing.");

            ICoffee coffee = new StandardCoffee { Ingredients = ingredients };
            coffee.Ingredients = ingredients;
            coffee.QualityRules = new RuleStandardCoffee();
            coffee.Recipe = recipe;
            coffee.CoffeeQuality = QualityControlInspector.InspectBeverageQuality(coffee);
            return coffee;
        }

        private void setRequiredComponents()
        {
            RequiredComponents = new List<IComponent>
            {
                new Carafe {MaxVolumeInCups = CarafeCapicity},
                new Filter {ScoopCapicity = ScoopCapicity}
            };
        }
    }
}
