﻿using Presentation.Consumables;
using Presentation.Recipes;
using Presentation.Rules;

namespace Presentation.Drinks
{
    public interface ICoffee
    {
        CoffeeQuality CoffeeQuality { get; set; }
        IIngredients Ingredients { get; set; }
        IRecipe Recipe { get; set; }
        IRule QualityRules { get; set; }
    }
}
