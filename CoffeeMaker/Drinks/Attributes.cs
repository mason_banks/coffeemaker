﻿namespace Presentation.Drinks
{
    public enum CoffeeQuality
    {
        Weak,
        Strong,
        Perfect,
        Unknown
    }
}