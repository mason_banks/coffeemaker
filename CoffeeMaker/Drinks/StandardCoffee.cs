﻿using Presentation.Consumables;
using Presentation.Recipes;
using Presentation.Rules;

namespace Presentation.Drinks
{
    public class StandardCoffee : ICoffee
    {
        public CoffeeQuality CoffeeQuality { get; set; }
        public IIngredients Ingredients { get; set; }
        public IRecipe Recipe { get; set; }
        public IRule QualityRules { get; set; }
    }
}
