﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Presentation.Components;
using Presentation.Consumables;
using Presentation.Exceptions;
using Presentation.Hardware.CoffeeMakers;
using Presentation.Hardware.Components;
using Presentation.Recipes;
using Shouldly;

namespace CoffeeMakerTest
{
    [TestClass]
    public class CoffeeMaker8CupTests
    {
        private readonly CoffeeMaker8Cup _properlyConfiguredCoffeeMaker8Cup;

        private CoffeeMakerComponents _coffeeMakerComponents;
        private CoffeeMakerComponents _coffeeMakerComponentsWithoutFilter;
        private CoffeeMakerComponents _coffeeMakerComponentsWithoutCarafe;
        private readonly StandardCoffeeRecipe _standardCoffeeRecipe;

        public CoffeeMaker8CupTests()
        {
            setupCoffeeMakerComponents();
            _standardCoffeeRecipe = new StandardCoffeeRecipe();
            _properlyConfiguredCoffeeMaker8Cup = new CoffeeMaker8Cup(_coffeeMakerComponents);
        }

        [TestMethod]
        public void make_cup_of_coffee()
        {
            var ingredients = new Ingredients
            {
                Collection = new List<IIngredient>
                {
                    new CoffeeGrounds {QuantityInScoops = 4},
                    new Water {VolumeInCups = 8}
                }
            };

            var coffee = _properlyConfiguredCoffeeMaker8Cup.Brew(_standardCoffeeRecipe, ingredients);

            coffee.ShouldNotBeNull();
        }

        [TestMethod]
        public void no_water_should_throw_exception()
        {
            var ingredients = new Ingredients
            {
                Collection = new List<IIngredient>
                {
                    new CoffeeGrounds {QuantityInScoops = 8}
                }
            };

            Should.Throw<Exception>(() =>
            {
                _properlyConfiguredCoffeeMaker8Cup.Brew(_standardCoffeeRecipe, ingredients);
            }).Message.ShouldBe("Ingredients for the requested recipe are missing.");
        }

        [TestMethod]
        public void no_grounds_should_throw_exception()
        {
            var ingredients = new Ingredients
            {
                Collection = new List<IIngredient>
                {
                    new Water {VolumeInCups = 8}
                }
            };

            Should.Throw<MissingIngredientsException>(() =>
            {
                _properlyConfiguredCoffeeMaker8Cup.Brew(_standardCoffeeRecipe, ingredients);
            }).Message.ShouldBe("Ingredients for the requested recipe are missing.");
        }

        [TestMethod]
        public void no_coffee_filter_should_throw_exception()
        {
            Should.Throw<ImproperlyConfiguredHardwareException>(() =>
            {
                var coffeeMakerWithoutFilter8Cup = new CoffeeMaker8Cup(_coffeeMakerComponentsWithoutFilter);
            }).Message.ShouldBe("The hardware is improperly configured.");

        }

        [TestMethod]
        public void no_carafe_should_throw_exception()
        {
            Should.Throw<Exception>(() =>
            {
                var coffeeMakerWithoutCarafe8Cup = new CoffeeMaker8Cup(_coffeeMakerComponentsWithoutCarafe);
            }).Message.ShouldBe("The hardware is improperly configured.");
        }

        [TestMethod]
        public void too_much_water_should_throw_exception()
        {
            int carafeWaterCapacity =
                ((Carafe) _properlyConfiguredCoffeeMaker8Cup.RequiredComponents.ToList()
                .Find(x => x.GetType() == typeof(Carafe))) .MaxVolumeInCups;

            var ingredients = new Ingredients
            {
                Collection = new List<IIngredient>
                {
                    new CoffeeGrounds {QuantityInScoops = 4},
                    new Water {VolumeInCups = carafeWaterCapacity + 1}
                }
            };

            Should.Throw<Exception>(() =>
            {
                var coffee = _properlyConfiguredCoffeeMaker8Cup.Brew(_standardCoffeeRecipe, ingredients);
            }).Message.ShouldBe("An ingredient has been added that exceeds or doesn't meet the requirements of the hardware.");
        }

        [TestMethod]
        public void too_much_coffee_grounds_should_throw_exception()
        {
            int filterCapacity =
                ((Filter)_properlyConfiguredCoffeeMaker8Cup.RequiredComponents.ToList()
                .Find(x => x.GetType() == typeof(Filter))).ScoopCapicity;

            var ingredients = new Ingredients
            {
                Collection = new List<IIngredient>
                {
                    new CoffeeGrounds {QuantityInScoops = filterCapacity + 1},
                    new Water {VolumeInCups = 8}
                }
            };

            Should.Throw<Exception>(() =>
            {
                var coffee = _properlyConfiguredCoffeeMaker8Cup.Brew(_standardCoffeeRecipe, ingredients);
            }).Message.ShouldBe("An ingredient has been added that exceeds or doesn't meet the requirements of the hardware.");
        }

        private void setupCoffeeMakerComponents()
        {
            _coffeeMakerComponents = new CoffeeMakerComponents
            {
                RequiredComponents = new List<IComponent> { new Carafe(), new Filter() }
            };

            _coffeeMakerComponentsWithoutFilter = new CoffeeMakerComponents
            {
                RequiredComponents = new List<IComponent> { new Carafe() }
            };

            _coffeeMakerComponentsWithoutCarafe = new CoffeeMakerComponents
            {
                RequiredComponents = new List<IComponent> { new Filter() }
            };
        }
    }
}
