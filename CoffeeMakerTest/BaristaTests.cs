﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Presentation.Actors;
using Presentation.Consumables;
using Presentation.Drinks;
using Presentation.Hardware.CoffeeMakers;
using Presentation.Recipes;
using Shouldly;

namespace CoffeeMakerTest
{
    [TestClass]
    public class BaristaTests
    {
        private readonly Mock<ICoffeeMakerModel> _mockCoffeeMaker;
        private readonly Mock<IIngredients> _stubIngredients;
        private readonly Mock<IRecipe> _stubRecipe;

        public BaristaTests()
        {
            _stubIngredients = new Mock<IIngredients>();
            _stubRecipe = new Mock<IRecipe>();

            _mockCoffeeMaker = new Mock<ICoffeeMakerModel>();

            _mockCoffeeMaker.Setup(coffeeMaker =>
                coffeeMaker.Brew(
                    It.IsAny<IRecipe>(),
                    It.IsAny<IIngredients>()))
                    .Returns(new StandardCoffee());

        }

        [TestMethod]
        public void barista_can_make_a_cup_of_coffee()
        {
            var barista = new Barista(_mockCoffeeMaker.Object);
            var coffee = barista.Brew(_stubRecipe.Object, _stubIngredients.Object);

            coffee.ShouldNotBeNull();
        }
    }
}
