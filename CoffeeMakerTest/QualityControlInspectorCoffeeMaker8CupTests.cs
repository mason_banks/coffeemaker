﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Presentation.Actors;
using Presentation.Components;
using Presentation.Consumables;
using Presentation.Drinks;
using Presentation.Hardware;
using Presentation.Hardware.CoffeeMakers;
using Presentation.Hardware.Components;
using Presentation.Recipes;
using Presentation.Rules;
using Shouldly;

namespace CoffeeMakerTest
{
    [TestClass]
    public class QualityControlInspectorCoffeeMaker8CupTests
    {
        private readonly CoffeeMaker8Cup _properlyConfiguredCoffeeMaker8Cup;
        private readonly IRecipe _recipe;
        private ICoffeeMakerComponents _coffeeMaker8CupComponents;

        public QualityControlInspectorCoffeeMaker8CupTests()
        {
            setupCoffeeMakerComponents();
            _recipe = new StandardCoffeeRecipe();
            _properlyConfiguredCoffeeMaker8Cup = new CoffeeMaker8Cup(_coffeeMaker8CupComponents);
        }

        [TestMethod]
        public void make_perfect_cup_of_basic_coffee()
        {
            var ingredients = new Ingredients
            {
                Collection = new List<IIngredient>
                {
                    new CoffeeGrounds {QuantityInScoops = 4},
                    new Water {VolumeInCups = 8}
                }
            };

            var coffee = _properlyConfiguredCoffeeMaker8Cup.Brew(_recipe, ingredients);
            coffee.QualityRules = new RuleStandardCoffee();
            QualityControlInspector.InspectBeverageQuality(coffee).ShouldBe(CoffeeQuality.Perfect);
        }

        [TestMethod]
        public void make_coffee_too_strong()
        {
            var ingredients = new Ingredients
            {
                Collection = new List<IIngredient>
                {
                    new CoffeeGrounds {QuantityInScoops = 8},
                    new Water {VolumeInCups = 8}
                }
            };

            var coffee = _properlyConfiguredCoffeeMaker8Cup.Brew(_recipe, ingredients);
            coffee.QualityRules = new RuleStandardCoffee();
            QualityControlInspector.InspectBeverageQuality(coffee).ShouldBe(CoffeeQuality.Strong);
        }

        [TestMethod]
        public void make_coffee_too_weak()
        {
            var ingredients = new Ingredients
            {
                Collection = new List<IIngredient>
                {
                    new CoffeeGrounds {QuantityInScoops = 2},
                    new Water {VolumeInCups = 8}
                }
            };

            var coffee = _properlyConfiguredCoffeeMaker8Cup.Brew(_recipe, ingredients);
            coffee.QualityRules = new RuleStandardCoffee();
            QualityControlInspector.InspectBeverageQuality(coffee).ShouldBe(CoffeeQuality.Weak);
        }

        private void setupCoffeeMakerComponents()
        {
            _coffeeMaker8CupComponents = new CoffeeMakerComponents
            {
                RequiredComponents = new List<IComponent> {new Carafe(), new Filter()}
            };
        }
    }
}
